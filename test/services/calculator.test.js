const { addTwoNumbers } = require("../../src/services/calculator");

test('add two numbers (4)(6) then 10', () => {
    expect(addTwoNumbers(4)(6)).toBe(10);
});