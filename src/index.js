const express = require('express')
const { addTwoNumbers } = require('./services/calculator')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/add-two-numbers', (req, res) => {
    const n1 = parseInt(req.query.n1);
    const n2 = parseInt(req.query.n2);

    const result = addTwoNumbers(n1)(n2);

    res.send({ result });
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})